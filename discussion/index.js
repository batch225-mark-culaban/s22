//mutated or iterated - pwede m balik
console.log( "hello");

// Array Methods - Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.


// Mutator Methods

/*
	-Mutator methods are functions that "mutate" or change an array after they'rer created

	- These methods manipulate the original array performing various task such as adding and removing elements. 

*/

//push()
/*
	-adds an element in the end of an array and returns the array's length.

	SYNTAX

	arrayName.push();

*/

let fruits = ['apple', 'orange', 'kiwi', 'dragon fruit'];

console.log('Current Array : ');
console.log(fruits);

let fruitsLength = fruits.push('mango');
console.log(fruitsLength);
console.log('Mutated Array from push method: ')
console.log(fruits);

//Adding multiple elements to an array
	fruits.push('avocado', 'guava');
	console.log('mutated array from push method: ')
	console.log(fruits);

// pop()
/*
	-removes the last element in an array AND returns the removed element
	-SYNTAX
		arrayname.pop();

*/
let removeDFruit = fruits.pop();
console.log(removeDFruit);
console.log('Mutated array from pop method: ');
console.log(fruits);



// Unshift()
/*
 -Adds one or more elements at the beginning of ann aray.

 SYNTAX
 	arrayName.unshift( 'elementA');
 	arrayName.unshift(' elementA', elementb);
 */

fruits.unshift('lime', 'banana');
console.log('Mutated from unshift method: ');
console.log(fruits);



// Shift(), pabalik sa na remove na element, 1 step.
/*
	-Removes an element at the beginning of an array and returns the removed element
SYNTAX
	arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method: ');
console.log(fruits);

// Splice()
/*
	-Simultaneously removes elements from a sprecified index number and adds element 
	
	SYNTAX
		arrayName.splice(startingIndex, deleteCount, elementsTobeAdded)

*/
//startingIndex - number kung asa element sugod --deleted
//deleteCount - hantud asa mu delete,counted from  0 = startingIndex; or number kung pila ka element idelete from starting index

//elements to be added-- add sa na delete
//REMINDER = basehan ng deleteCount ung startingIndex;
//pwede pd remove lng,

const try1 = fruits.splice(1, 2, 'Lime', 'cherry', 'lemon'); //console.log sa na delete na elements

fruits.splice(1, 2, 'cherry', 'lemon');
console.log(try1);
console.log('Mutated array from splice method');
console.log(fruits);

// sort()
/*	
	-Rearrange the array elements in alphanumeric order.

SYNTAX
	arrayName.sort();
*/
const randomThings = ['Cat', 'boy', 'apps', 'zoo']
//alphanumeric pero priority and UPPERCASE;
//result = Cat, apps, boy, zoo

randomThings.sort();
console.log('Mutated array from sort method: ');
console.log(randomThings);


// reverse()

/* 
	-Reverse the order of array elements
	
	SYNTAX
		arrayName.reverse();

*/

randomThings.reverse();
console.log('Mutated array from reverse method: ');
console.log(randomThings);


//	NON-MUTATORS METHODS

/*
	-  Non-mutator methods are functions that do not modify or change an array after they're created
	-	These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing output.
*/

// indexOf()

/*
	- Returns the index number of the first matching element found in an array
	- If no match was found, the result will be -1
	- The search process will be done from first element proceeding to the last element.

	SYNTAX:
		arrayName.indexOf(searchValue)
*/
let countries = [ 'US', 'PH', 'CAN', 'SG', 'TH', 'BR', 'FR', 'DE'];

let firstIndex = countries.indexOf('CAN');
console.log('Result of indexOf method: ' + firstIndex);

//determines what number the element is 

// slice()
/*
	- Portion/slices elements from an array AND returns a new array.
	
	SYNTAX
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
		
*/
// SLICING OFF ELEMENTS FROM A SPECIFIED INDEX TO THE FIRST ELEMENT

let slicedArrayA = countries.slice(4);// = remaining 4 - starting from 4th element to last
console.log('result from sliced method A: ' );

console.log(slicedArrayA);
console.log(countries);//  does not changed original array
/////////////////////////////////////////////////////////


//Slicing off elements from a specified index to another index
//Note: the last element is not included.
let slicedArrayB = countries.slice(0, 3); 
// ang numbers range sa m stay, ex sa 0, 3 (stay = 0 - 2) or number 3 is not included


console.log('result from sliced method B');
console.log(countries);
console.log(slicedArrayB);




//////////////////////////////////////////////////////////
// slicing off elements starting from the LAST element of an array

let slicedArrayC = countries.slice(-3);// use negative to start from last element

console.log('Result from slice method C: ');
console.log(slicedArrayC);
//REMINDER, result = array remaining are from last element to -3 elements.




// forEach();

/*
	- Similar to for loop that iterates on each array element.
	- For each item in the array, the anonymous function passed in forEach() method will be run.
	- it must have "function";

	SYNTAX
		arrayName.forEach(function(indivElement)){
			statement
		}
*/


countries.forEach(function(country) {
	console.log(country);//good practice use singular for input, for easy read
});
//RESULT: assigns array element each line 
//element 1
//element 2
//etc,...



// includes()

/*
	includes()

	- includes() method checks if the argument passes can be found in the array.
	- it returns a boolean which can be saved in a variable.
		- return true if the arument is found in the array
		- return false if it is not.

		SYNTAX
			arrayName.includes(<argumentToFind>)
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

console.log(productFound1);//returns true

let productFound2 = products.includes("Headset");

console.log(productFound2);//returns false

//to find the argument in the array